#!/bin/bash

set -eu

################################################################################
# DEFAULTS
################################################################################

COMMAND=zsh
CONTAINER_USER=$(id -u)
TAG=latest
IMAGE_NAME=exadra37/java-dev-cli
DOCKER_PATH=~/bin/vendor/exadra37-docker/java/dev-cli/docker
BACKGROUND_MODE="-it"
PORT_MAP="8080:8080"


################################################################################
# PARSE INPUT
################################################################################

  if [ -f .env ]; then
      source .env
  fi

  for argument in "${@}"; do
    case "${argument}" in
      -d | --detached )
          BACKGROUND_MODE="--detached"
          shift 1
          ;;

      -it )
          BACKGROUND_MODE="-it"
          shift 1
          ;;

      -p | --publish )
          PORT_MAP=${2? Missing host port map for the container, eg: 8080:8080 !!!}
          shift 2
          ;;

      -t | --tag )
          TAG="${2? Missing tag for docker image!!!}"
          shift 2
          ;;

      -u | --user )
          CONTAINER_USER=${2? Missing suer for container!!!}
          shift 2
          ;;

      build )
          sudo docker build \
              -t ${IMAGE_NAME}:${TAG} \
              "${DOCKER_PATH}"/build

          exit 0
          ;;

      shell )
          COMMAND=zsh
          shift 1
          ;;

    esac
  done


################################################################################
# EXECUTION
################################################################################

  sudo docker run \
    --rm \
    ${BACKGROUND_MODE} \
    --user ${CONTAINER_USER} \
    --publish "127.0.0.1:"${PORT_MAP} \
    --volume "${PWD}":/home/java/workspace \
    ${IMAGE_NAME}:${TAG} \
    "${COMMAND}" "${@}"
